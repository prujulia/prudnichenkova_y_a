#include <iostream>
#include <opencv2/opencv.hpp>
using  namespace cv;
using namespace std;
int main()
{
    //1st part
    Mat image1 = imread("../cross.png");
    cvtColor(image1, image1, COLOR_BGR2GRAY);
    imshow("image1", image1);
    imwrite("../part1.jpeg", image1);

    //2nd part
    int hist1[256];
    for (int i = 0; i <= 255; i += 1)
        hist1[i] = 0;

    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist1[image1.at<uchar>(i, j)] += 1;
        }

    int max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist1[i] > max_count_pixel)
            max_count_pixel = hist1[i];
    }

    Mat image2 = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(image2, Point(i - 1, hist1[i - 1]), Point(i, hist1[i]), 255, 1, 1, 0);
    }
    imshow("hist1", image2);
    imwrite("../part2.jpeg", image2);

    //3rd part
    Mat graph1 = Mat(256, 256, CV_8UC3, Scalar(255, 255, 255));
    array<uchar, 256> val;
    for (int i = 0; i <= 255; i += 1)
    {
        if (i <= 64)
        {
            val.at(i) = i;
        }
        else
        {
            val.at(i) = abs(i - 2 * abs(i - 64));
        }
    }
    for (int i = 1; i <= 255; i++)
    {
        line(graph1, Point(i - 1, val[i - 1]), Point(i, val[i]), 255, 1, 1, 0);
    }
    imshow("graph1", graph1);
    imwrite("../part3.jpeg", graph1);

    //4th part
    Mat image3;
    LUT(image1, val, image3);
    int hist2[256];
    for (int i = 0; i <= 255; i += 1)
        hist2[i] = 0;
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist2[image3.at<uchar>(i, j)] += 1;
        }
    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist2[i] > max_count_pixel)
            max_count_pixel = hist2[i];
    }
    Mat image4 = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(image4, Point(i - 1, hist2[i - 1]), Point(i, hist2[i]), 255, 1, 1, 0);
    }
    imshow("image2", image3);
    imshow("hist2", image4);
    imwrite("../part4-1.jpeg", image3);
    imwrite("../part4-2.jpeg", image4);

    //5th part
    Mat clahe_image;
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(3);
    clahe->setTilesGridSize(Size(10, 10));
    clahe->apply(image1, clahe_image);
    int hist3[256];
    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;
    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }

    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }

    Mat clahe_hist = Mat(max_count_pixel, 256, CV_8UC3, Scalar(255, 255, 255));
    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe1", clahe_image);
    imshow("hist_clahe1", clahe_hist);
    imwrite("../part5-3.jpeg", clahe_image);
    imwrite("../part5-4.jpeg", clahe_hist);

    clahe->setClipLimit(10);
    clahe->setTilesGridSize(Size(10, 10));
    clahe->apply(image1, clahe_image);

    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;

    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }

    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }

    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe2", clahe_image);
    imshow("hist_clahe2", clahe_hist);
    imwrite("../part5-5.jpeg", clahe_image);
    imwrite("../part5-6.jpeg", clahe_hist);

    clahe->setClipLimit(0.5);
    clahe->setTilesGridSize(Size(15, 15));
    clahe->apply(image1, clahe_image);

    for (int i = 0; i <= 255; i += 1)
        hist3[i] = 0;

    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            hist3[clahe_image.at<uchar>(i, j)] += 1;
        }

    max_count_pixel = 0;
    for (int i = 0; i <= 255; i += 1)
    {
        if (hist3[i] > max_count_pixel)
            max_count_pixel = hist3[i];
    }

    for (int i = 1; i <= 255; i++)
    {
        line(clahe_hist, Point(i - 1, hist3[i - 1]), Point(i, hist3[i]), 255, 1, 1, 0);
    }
    imshow("clahe3", clahe_image);
    imshow("hist_clahe3", clahe_hist);
    imwrite("../part5-1.jpeg", clahe_image);
    imwrite("../part5-2.jpeg", clahe_hist);

    //6th part
    Mat bin_image(256, 256, CV_8UC1);

    int m = 0;
    int n = 0;

    int threshold = 0;

    int alpha1 = 0;
    int beta1 = 0;

    float dispersion = 0;
    float max_dispersion = 0;

    float w1 = 0, w2 = 0;
    float a = 0;

    for (int t = 0; t <= 255; t += 1)
    {
        m += t * hist1[t];
        n += hist1[t];
    }

    for (int t = 0; t <= 255; t += 1)
    {
        alpha1 += t * hist1[t];
        beta1 += hist1[t];
        w1 = (float)beta1 / n;

        a = (float)alpha1 / beta1 - (float)(m - alpha1) / (n - beta1);

        dispersion = w1 * (1 - w1) * a * a;

        if (dispersion > max_dispersion)
        {
            max_dispersion = dispersion;
            threshold = t;
        }
    }

    for (int i = 0; i <= 255; i += 1)
        for (int j = 0; j <= 255; j += 1)
        {
            if (image1.at<uchar>(i, j) < threshold)
                bin_image.at<uchar>(i, j) = 0;
            else
                bin_image.at<uchar>(i, j) = 255;
        }

    Mat res_image;
    hconcat(image1, bin_image, res_image);
    imshow("bin_image", res_image);
    imwrite("../part6.jpeg", res_image);

    //7th part
    Mat bin_adaptive_image;
    adaptiveThreshold(image1, bin_adaptive_image, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 29, 9);
    Mat res_adaptive_image;
    hconcat(image1, bin_adaptive_image, res_adaptive_image);
    imshow("bin_adaptive_image", res_adaptive_image);
    imwrite("../part7.jpeg", res_adaptive_image);

    //8th part
    Mat filter_image;
    Mat res_filter_image;
    morphologyEx(bin_image, filter_image, MORPH_OPEN, Mat(), Point(-1, 1), 1, 0);
    hconcat(bin_image, filter_image, res_filter_image);
    imshow("filter_image", res_filter_image);
    imwrite("../part8.jpeg", res_filter_image);

    //9th part
    Mat alpha_image;
    double alpha = 0.7;
    addWeighted(image1, alpha, filter_image, 1 - alpha, 0.0, alpha_image);
    imshow("alpha_image", alpha_image);
    imwrite("../part9.jpeg", alpha_image);

    waitKey(0);
    return 0;
}
