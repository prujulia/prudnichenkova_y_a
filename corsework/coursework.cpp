#include <opencv2/highgui/highgui.hpp>
#include <opencv2/photo/photo.hpp>
#include <iostream>

cv::Mat adjust_covariance(cv::Mat Lab[3], cv::Mat sLab[3])
{
    float tcrosscorr, scrosscorr;
    cv::Mat temp1;
    cv::Scalar smean, sdev, temp2;

    cv::multiply(Lab[1],Lab[2],temp1);
    temp2=cv::mean(temp1);
    tcrosscorr =temp2[0];

    std::cout<<tcrosscorr<<" =tcrosscorr \n";

    cv::multiply(sLab[1],sLab[2],temp1);
    temp2=cv::mean(temp1);
    scrosscorr =temp2[0];
    std::cout<<scrosscorr<<" =scrosscorr \n";

    cv::Mat z1=Lab[1].clone();
    cv::Mat z2=Lab[1].clone();
    z1=0.5*(Lab[1]+Lab[2])/sqrt((1+tcrosscorr)/2);
    z2=0.5*(Lab[1]-Lab[2])/sqrt((1-tcrosscorr)/2);

    Lab[1]=sqrt((1+scrosscorr)/2)*z1+sqrt((1-scrosscorr)/2)*z2;
    Lab[2]=sqrt((1+scrosscorr)/2)*z1-sqrt((1-scrosscorr)/2)*z2;

    cv:merge(Lab,3,temp1);
    return temp1;

}

cv::Mat Rescale(cv::Mat lab_image)
{

    cv::Mat Lab[3];
    cv::Point minLoc, maxLoc;
    double minVal, maxVal, scale=0.0, maxDev;

    split(lab_image,Lab);

    cv::minMaxLoc(Lab[1],&minVal, &maxVal, &minLoc, &maxLoc );
    scale=std::max(0.0,maxVal/127);
    scale=std::max(scale,-minVal/127);

    cv::minMaxLoc(Lab[2],&minVal, &maxVal, &minLoc, &maxLoc );
    scale=std::max(scale, maxVal/127);
    scale=std::max(scale,-minVal/127);

    std::cout<<"   "<<   scale << " scale\n";

    if (scale>1.0) {Lab[1]=Lab[1]/scale; Lab[2]=Lab[2]/scale;}

    cv::minMaxLoc(Lab[0],&minVal, &maxVal, &minLoc, &maxLoc );
    scale=std::max((maxVal-50)/50, -(minVal-50)/50);
    if(scale>1) {Lab[0]=(Lab[0]-50)/scale+50;}

    merge(Lab,3,lab_image);

    return lab_image;
}


int main(int argc, char *argv[])
{

    bool CrossCovarianceProcessing = true;
    bool KeepOriginalShading       = true;
    bool ScaleRatherThanClip       = true;
    int  iterations                = 2;

    std::string targetname = "../img_1.jpg";
    std::string sourcename = "../img_2.jpg";

    cv::Mat targetf, sourcef, Lab[3], sLab[3];
    cv::Scalar tmean, tdev, smean, sdev;
    cv::Mat saved;// !!!

    cv::Mat target = cv::imread(targetname, 1);
    cv::Mat source = cv::imread(sourcename, 1);

    target.convertTo(targetf,CV_32FC3,1/255.0);
    source.convertTo(sourcef,CV_32FC3,1/255.0);

    cv::cvtColor(sourcef, sourcef, CV_BGR2Lab);
    cv::meanStdDev(sourcef, smean, sdev);
    cv::split(sourcef,sLab);
    sLab[1]=(sLab[1]-smean[1])/sdev[1];
    sLab[2]=(sLab[2]-smean[2])/sdev[2];

    for (int i=0;i<iterations;i++)
    {
        cv::cvtColor(targetf, targetf, CV_BGR2Lab);
        cv::meanStdDev(targetf, tmean, tdev);
        cv::split(targetf,Lab);
        Lab[1]=(Lab[1]-tmean[1])/tdev[1];
        Lab[2]=(Lab[2]-tmean[2])/tdev[2];

        if(CrossCovarianceProcessing)
        {
            targetf=adjust_covariance(Lab, sLab);
            cv::split(targetf,Lab);
        }

        Lab[1]=Lab[1]*sdev[1]+smean[1];
        Lab[2]=Lab[2]*sdev[2]+smean[2];

        if(!KeepOriginalShading)
        {
            Lab[0]=(Lab[0]-tmean[0])/tdev[0];
            Lab[0]=Lab[0]*sdev[0]+smean[0];
        }

        cv::merge(Lab,3,targetf);

        if(ScaleRatherThanClip){targetf=Rescale(targetf);}

        cv::cvtColor(targetf, targetf,CV_Lab2BGR);
    }

    targetf.convertTo(target,CV_8UC3,255.0);

    cv::imshow("processed image",target);
    cv::imwrite("images/result.jpg", target);

    cv::waitKey(0);
    return 0;
}
