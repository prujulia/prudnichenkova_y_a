cmake_minimum_required(VERSION 3.16)
project(HATE)
set(CMAKE_CXX_STANDARD 14)
find_package(OpenCV REQUIRED)
add_executable(HATE lab01.cpp)
target_link_libraries(HATE PRIVATE ${OpenCV_LIBS})