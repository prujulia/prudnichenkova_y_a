#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <iostream>

using  namespace cv;
using namespace std;

vector<vector<float>> kernel_matrix = {
    {-1, 0, 1},
    {-2, 0, 2},
    {-1, 0, 1}
};

string image_dir = "results/";

Mat get_kernel(bool transposed=false) {
    Mat kernel(3, 3, CV_32F);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (transposed) {
                kernel.at<float>(i, j) = kernel_matrix[j][i];
            } else {
                kernel.at<float>(i, j) = kernel_matrix[i][j];
            }
        }
    }

    return kernel;
}

void assignment1() {
    vector<double> colors = {0, 0.5, 1};

    int type = CV_32F;
    Mat r1(200, 200, type), r2(200, 200, type), r3(200, 200, type),
        r4(200, 200, type), r5(200, 200, type), r6(200, 200, type);

    vector<int> bcolor_indexes = {0, 1, 2, 2, 0, 1};
    vector<int> ccolor_indexes = {2, 0, 1, 0, 1, 2};
    vector<Mat> pieces = {r1, r2, r3, r4, r5, r6};

    for (int i = 0; i< 6; i++) {
        pieces[i].setTo(colors[bcolor_indexes[i]]);
        circle(pieces[i], Point(100, 100), 80, colors[ccolor_indexes[i]], -1);
    }

    vector<Mat> top_v = {r1, r2, r3};
    vector<Mat> bottom_v = {r4, r5, r6};

    Mat top;
    hconcat(top_v, top);

    Mat bottom;
    hconcat(bottom_v, bottom);

    Mat result;
    vconcat(top, bottom, result);

    imshow("result", result);
    imwrite("../result.png", 255 * result);

    Mat kernel1 = get_kernel();
    Mat first_filtred;
    filter2D(result, first_filtred, -1, kernel1, Point(-1, -1), 0, cv::BORDER_DEFAULT);
    first_filtred += 4;
    first_filtred /= 8;

    imshow("filter1", first_filtred);
    imwrite("../filter1.png", 255 * first_filtred);


    Mat kernel2 = get_kernel(true);
    Mat second_filtred;
    filter2D(result, second_filtred, -1, kernel2, Point(-1, -1), 0, cv::BORDER_DEFAULT);
    second_filtred += 4;
    second_filtred /= 8;

    imshow("filter2", second_filtred);
    imwrite("../filter2.png", 255 * second_filtred);

    Mat squared_res1, squared_res2, third_filtred;
    pow(first_filtred, 2, squared_res1);
    pow(second_filtred, 2, squared_res2);
    sqrt(squared_res1 + squared_res2, third_filtred);

    imshow("filter3", third_filtred);
    imwrite("../filter3.png", 255 * third_filtred);

    waitKey(0);
}

int main() {
    assignment1();
}
